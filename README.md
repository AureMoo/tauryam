# Tauryam

ISN Project during my last year of High School (2018). <br>
*Lycée La Xavière, Lyon 8, France, General Scientist "Terminale" with IT option (Informatique et Sciences du Numérique in French), 2018*

## Description

Tauryam, a Star, needs to deliver his mother but she has a difficult journey and you have to help her. <br>
Two worlds are here to block your way. One who seems to be like a "Mario Bros" and the second like an "Invaders". <br>

## Team

Aurélien Moote : Project Manager and Mario Bros developer <br>
Myriam B. : Invaders developer <br>
Thomas P. : Menu developer <br>

Thanks to Thierry B. for his help.

## Installation

Need Python or Python3

pip(or pip3) installations: <br>
```
pip download pygame <br>
pip install pygame <br>
brew install python-tk <br>
```

## Launch Game

The menu has some errors but you can still launch it if you want. <br>
Go on the root folder of the project.

```
python3 init.py
```

If you want to just play to the game, write this :

```
python3 thegame.py
```

The "Invaders" is with some bugs so it is desactivated at the moment