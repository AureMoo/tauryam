#Fenêtre principale
# -*- coding: utf-8 -*-

#importations
from thegame import *

from tkinter import * 
from tkinter.messagebox import *

'''def alert():
    showinfo("alerte", "Bravo!")'''

def off():      # la fenêtre se ferme
    fenetre.destroy()

def callback():     
    if  askyesno('Avertissement', 'Êtes-vous sûr de vouloir faire ça?'):
        showwarning('Menace', "vous allez le regretter...")
        fenetre.destroy()
    else:
        showinfo('Seconde chance', 'Sage décision')
        showerror("Le Retour", "Il est temps de montrer ce dont vous êtes capable")

def showplay():
    if  coche.get()==0:         #si le bouton "ready" est décoché, on cache "play" 
        #Hide Button
        play.pack_forget()
    else:       #si le bouton "ready" est coché, on montre le bouton "play"
        #Show Button
        play.pack()

#fenetre
fenetre = Tk()
fenetre.title("Tauryam")
fenetre.geometry("1250x900+0+0")

# canvas
canvas = Canvas(fenetre, width=200, height=60, background='blue')
txt = canvas.create_text(100, 30, text="TORYAM", font="Arial 16 italic", fill="yellow")
canvas.pack()

canvas = Canvas(fenetre, width=1000, height=80, background='red')
txt = canvas.create_text(500, 40, text="Bienvenue dans le monde de Tauryam ! Visiteur, es-tu prêt ?", font="Arial 16 italic", fill="yellow")
canvas.pack()

canvas = Canvas(fenetre, width=1400, height=50, background='yellow')
txt = canvas.create_text(700, 30, text="Tu vas donc aider cette étoile à retrouver sa galaxie ainsi que sa famille. Nous te souhaitons bonne chance.", font="Arial 16 italic", fill="black")
canvas.pack()

# checkbutton
coche=IntVar()
bouton = Checkbutton(fenetre, text="Ready",relief=GROOVE, cursor="trek", variable=coche, command=showplay)
bouton.pack()

label = Label(fenetre, text="Une étoile s'est égarée de sa galaxie natale et elle cherche à retrouver sa famille. Pour l'aider, tu devras surmonter 2 épreuves.")
label.pack()

Button(text='Abandonner', command=callback).pack()

# bouton de sortie
bouton=Button(fenetre, text="Fermer", command=off)
bouton.pack()

#
play=Button(text ="PLAY", relief=RAISED, cursor="heart",command=JEUX)

photo = PhotoImage(file='m104.gif')
txt = Text(fenetre)
txt.pack()
txt.insert(END, "Galaxie du Sombrero")
txt.image_create(1.19, image=photo)

fenetre['bg']='black'

fenetre.mainloop()
