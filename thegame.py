# '-*-coding:utf-8 -*-'

import pygame
from pygame.locals import*      #importation des bibliothèques nécéssaires
from math import*       #importe toutes les valeurs mathématiques telles que pi...
from random import randrange        #apporte les notions aléatoires
import sys      #Ce module fournit un accès à certaines variables utilisées et maintenues par l’interpréteur, et à des fonctions interagissant fortement avec ce dernier. 
import os       #Le module OS dans Python fournit un moyen d'utiliser le système d'exploitation dépendant fonctionnalité. 
import time         #pour importer la fonction du temps
'''import emoji'''

#definition de quelques fonctions
#saut -> not used
def saut(perso,position_perso):
    position_perso=position_perso.move(2,-10)         # Saut du personnage vers la droite
    pygame.time.delay(5)
    fenetre.blit(perso,position_perso)
    return position_perso

def desaut(perso,position_perso):
    position_perso=position_perso.move(2,10)        # faire retomber le personnage après le saut vers la droite
    pygame.time.delay(5)
    fenetre.blit(perso,position_perso)
    return position_perso

def saut1(perso,position_perso):
    position_perso=position_perso.move(-2,-10)         # Saut du personnage vers la gauche
    pygame.time.delay(5)
    fenetre.blit(perso,position_perso)
    return position_perso

def desaut1(perso,position_perso):
    position_perso=position_perso.move(-2,10)        # faire retomber le personnage après le saut vers la gauche
    pygame.time.delay(5)
    fenetre.blit(perso,position_perso)
    return position_perso
  
#chronomètre
def chronometre(Start_time):
    Actual_time=time.time()-Start_time      # On importe le temps du début et on le soustrait au temps actuel 
    return Actual_time

def aff_chrono(Actual_time):
    font = pygame.font.SysFont("Tahoma",40)
    text = font.render(str(round(Actual_Time,3)),1, (228, 240, 0))
    textpos = text.get_rect()
    textpos.centerx = fenetre.get_rect().centerx
    fenetre.blit(text, textpos)
    
#pièges
def depiege(piege,pos_piege):       #Fonction permettant de faire défiler les pièges
    pos_piege=pos_piege.move(int(round(-10*width/1000)),0)
    fenetre.blit(pygame.transform.scale(piege, (int(round(154*width/1000)),int(round(154*height/600)))),pos_piege)
    return pos_piege 

def reset_piege(pos_piege):     #Fonction permettant de remmetre le piège au début
    return pygame.Rect(1000*width/1000,435*height/600,154*width/1000,154*height/600),0
 
def active_piege(PiegeActif):       #Fonction permettant d'activer ou non le piège
    if PiegeActif==0:
        if randrange(0,3)==1: 
            return 1
        else:
            return 0
    return 1
    
#défilement
def defilement(fond,fondrect,fondrectbis): 
    fondrect = fondrect.move (int(round(-10*width/1000)),0)        #On déplace les deux fonds en fonction des pièges
    fondrectbis = fondrectbis.move(int(round(-10*width/1000)),0)
    if fondrect.x < -width:         
       fondrect = pygame.Rect(width,0,width,height)
    if fondrectbis.x < -width:
       fondrectbis = pygame.Rect(width,0,width,height)
    #if fondrect == pygame.Rect(-width,0,width,height):         
    #   fondrect = pygame.Rect(width,0,width,height)
    #if fondrectbis == pygame.Rect (-width,0,width,height):
    #   fondrectbis = pygame.Rect (width,0,width,height)
    fenetre.blit(fond, fondrect)
    fenetre.blit(fond, fondrectbis)
    return fondrect,fondrectbis

continuer = 1   #variable qui contine les boucles de jeux si =1 et stop si =0
gagner=0        #variable qui definit si le joueur a gagné ou pas 0=perdu et 1=gagné
TempsdejeuxMarioBros=20
TempsdejeuxInvaders=20



#debut
try:
##################################################################################################################################################################
#"Mario-Bros"

    pygame.init()       #initialisation de la bibliotèque pygame
    pygame.font.init()      #initialise les polices de caractère

    width=1000
    height=600
    fenetre=pygame.display.set_mode((width,height),RESIZABLE)      #création d'une fenêtre avec possibilité de redimensionner
    pygame.display.set_caption("Tauryam")         #titre de la fenetre

    # Charger et coller les images et le son 
    fond=pygame.image.load("images/fondsol-mario.jpg").convert()     #chargement d'un arrière-plan
    fenetre.blit(pygame.transform.scale(fond, (width, height)),(0,0))        
    
    perso=pygame.image.load("images/Etoile_2_1-mario.png").convert()     #importation de l'image du personnage
    perso.set_colorkey((255,255,255))       #rendre son arrière-plan transparant
    #h=height*379/600
    position_perso=pygame.Rect(0,379*height/600,100*width/1000,96*height/600)        #position du perso(Abscisse, ordonnée, longeur, hauteur)   
    fenetre.blit(pygame.transform.scale(perso, (int(round(100*width/1000)),int(round(96*height/600)))),position_perso)     #chargement et collage d'un personnage
    
    piege=pygame.image.load("images/trounoir1-mario.png")
    piege=piege.convert_alpha(piege)
    #h2=435*height/600
    pos_piege=pygame.Rect(width,435*height/600,154*width/1000,154*height/600)

    son_depart=pygame.mixer.Sound("sounds/Dead_Silence_Soundtrack.wav")        #import d'un son
    son_depart.play(loops=1, maxtime=0, fade_ms=0)      #le son tourne en boucle (1 loop)
 
    # On position les deux fonds, qui sont les mêmes
    fondrect = pygame.Rect(0,0,width,height)
    fondrectbis = pygame.Rect(width,0,width,height)
    fenetre.blit(pygame.transform.scale(fond, (width, height)), fondrect)
    fenetre.blit(pygame.transform.scale(fond, (width, height)), fondrectbis) 

    pygame.display.flip()       #rafraichir l'écran

    pygame.key.set_repeat(400,30)        #effectuer le déplacement plusieurs fois en laissant la touche enfoncée (retard,intervalle)
 
    PiegeActif= 0
    vitesse=60 
    rapidite= 0         #delais entre deux pièges

    #Variable de saut du perso
    HauteurSautDroit=0
    HauteurSautGauche=0
    HauteurMax=20

    Start_Time=time.time()  # On démarre le chronomètre
    
    while continuer:        #boucle principale

        fondrect,fondrectbis=defilement(pygame.transform.scale(fond, (width, height)),fondrect, fondrectbis)
        PiegeActif=active_piege(PiegeActif)
        
        if PiegeActif==1 and pos_piege.x>-50*width/1000:       # On vérifie si le piege est présent
            pos_piege=depiege(piege,pos_piege)
        elif pos_piege.x<-40*width/1000 and PiegeActif==1:      # On vérifie si le piège n'est plus sur l'écran
            pos_piege,PiegeActif=reset_piege(pos_piege)
            rapidite=rapidite+1
        if rapidite==2:         # Lorsqu'on passe 5 piège, la vitesse augmente
            vitesse=int(vitesse*(9/10))
            rapidite=0

        #-------------------
        # Gestion Saut Droit
        #-------------------
        if HauteurSautDroit>0 and HauteurSautGauche==0:
            if HauteurSautDroit<((HauteurMax/2)+1):
                position_perso=position_perso.move(10*width/1000,-10*height/500)
                HauteurSautDroit=HauteurSautDroit+1
            else:
                position_perso=position_perso.move(10*width/1000,10*height/500)
                HauteurSautDroit=HauteurSautDroit+1
                
        if HauteurSautDroit>HauteurMax:
            HauteurSautDroit=0
            
        #-------------------
        # Gestion Saut Gauche
        #-------------------
        if HauteurSautGauche>0 and HauteurSautDroit==0:
            if HauteurSautGauche<((HauteurMax/2)+1):
                position_perso=position_perso.move(-2*width/1000,-10*height/600)
                HauteurSautGauche=HauteurSautGauche+1
            else:
                position_perso=position_perso.move(-2*width/1000,10*height/600)
                HauteurSautGauche=HauteurSautGauche+1
                
        if HauteurSautGauche>HauteurMax:
            HauteurSautGauche=0

        fenetre.blit(pygame.transform.scale(perso, (int(round(100*width/1000)),int(round(96*height/600)))),position_perso) # On recolle le stick sur le fond
        
        Actual_Time=chronometre(Start_Time)
        aff_chrono(Actual_Time) # On affiche le temps
        pygame.display.flip()
        pygame.time.delay(vitesse)

        #-------------------
        # Gestion Conflit
        #-------------------
        
        #Coordonnee du coin en bas à gauche et du coin en bas à droite dur perso
        XBGperso=position_perso.x*width/1000 
        YBGperso=position_perso.y*height/600+106.5*height/600 #Pourquoi 106.5 ... aprés test
        XBDperso=position_perso.x*width/1000+66*width/1000 #100-1/3
        YBDperso=position_perso.y*height/600+106.5*height/600 #Pourquoi 106.5 ... aprés test
        
        #Coordonnee du coin en haut à gauche et du coin en haut à droite dur piege
        XHGpiege=pos_piege.x*width/1000+51*width/1000 #154/3
        YHGpiege=pos_piege.y*height/600
        XHDpiege=pos_piege.x*width/1000+102*width/1000 #2*154/3
        YHDpiege=pos_piege.y*height/600


        if XBDperso > XHGpiege and YBDperso > YHGpiege and XBGperso < XHDpiege and YBGperso > YHDpiege:
        #if pos_piege.x == position_perso.x and int(round(position_perso.y))==int(round(379*height/600)) or pos_piege.x+int(round(130*width/1000)) == int(round(position_perso.x)) and int(round(position_perso.y))==int(round(379*height/600)): # On vérifie si le joueur ne tombe pas dans le trou
            print("Vous avez perdu...")
            '''print(emoji.emojize("U+1F608"))'''
            continuer=0
            Temps=round(Actual_Time,2)
            fichier=open("record-mario.txt","r")
            fichier.read()
            fichier.seek(0)
            score=fichier.readline()
            if float(Temps)>float(score):
                fichier.seek(0)
                print("Votre record : ",Temps)
                print("Ancien record : ",fichier.read())
                score=Temps
                score=str(score)
                fichier.close()
                fichier=open("record-mario.txt","w")
                fichier.write(score)
                fichier.close()
            else:
                print("Votre score : ",Temps)
                fichier.seek(0)
                print("Record actuel :",fichier.read())
                fichier.close()
                
        
        #-------------------
        # Gestion des evenements
        #-------------------
        for event in pygame.event.get():        #parcours de la liste de tous les évènements reçus
            if event.type == pygame.VIDEORESIZE:
                width0=width
                height0=height
                
                width, height = event.size
                
                fenetre=pygame.display.set_mode((width,height),RESIZABLE)

                if fondrect.x < 0:         
                    fondrect = pygame.Rect(fondrect.x,0,width,height)
                    fondrectbis = pygame.Rect(fondrect.x+width,0,width,height)
                if fondrectbis.x < 0:
                    fondrectbis = pygame.Rect(fondrectbis.x,0,width,height)
                    fondrect = pygame.Rect(fondrectbis.x+width,0,width,height)
                fenetre.blit(pygame.transform.scale(fond, (width, height)), fondrect)
                fenetre.blit(pygame.transform.scale(fond, (width, height)), fondrectbis) 

                position_perso=pygame.Rect(position_perso.x*width/1000,379*height/600,100*width/1000,96*height/600)        #position du perso(Abscisse, ordonnée, longeur, hauteur)   
                fenetre.blit(pygame.transform.scale(perso, (int(round(100*width/1000)),int(round(96*height/600)))),position_perso)     #chargement et collage d'un personnage

                if pos_piege.x < width:
                    pos_piege=pygame.Rect(pos_piege.x*width/1000,435*height/600,154*width/1000,154*height/600)
                else:
                    pos_piege=pygame.Rect(pos_piege.x*width/width0,435*height/600,154*width/1000,154*height/600)
                    
                fenetre.blit(pygame.transform.scale(piege, (int(round(154*width/1000)),int(round(154*height/600)))),pos_piege)

            if event.type == QUIT:        #si un évènement est QUIT...
                continuer =0        #...on arrête la boucle
            if event.type == KEYDOWN:       #si un évènement est dû à une touche de clavier
                if event.key == K_ESCAPE:   #si on appuie sur la touche echap l'écran se ferme
                    continuer = 0

                if event.key == K_RIGHT:       #si "flèche de droite"
                   position_perso = position_perso.move(10*width/1000,0)         #on décale vers la droite le perso de 10 pixels
                   
                if event.key == K_LEFT:
                   position_perso = position_perso.move(-10*width/1000,0)        #on décale vers la gauche le perso de 10 pixels
                   
                if event.key == K_SPACE:    #si "espace"
                    # si perso n est pas en saut (HauteurSautDroit=0) alors en saut (HauteurSautDroit=1)
                    if HauteurSautDroit==0 and HauteurSautGauche==0:
                        HauteurSautDroit=1
                                
                if event.key == K_RSHIFT:       #si "shift" de droite
                    # si perso n'est pas en saut (HauteurSautGauche=0) alors en saut (HauteurSautGauche=1)
                    if HauteurSautDroit==0 and HauteurSautGauche==0:
                        HauteurSautGauche=1
                        
                    

                while event in pygame.event.get():
                   continue                         # Vide l'évènement afin de ne pas sauter à répétition

        position_perso.left = max(0,position_perso.left)            #limites pour que le perso ne sorte pas de l'écran à gauche
        position_perso.right = min(width,position_perso.right)           #limites pour que le perso ne sorte pas de l'écran à droite

        #-------------------
        # Gestion temps d execution
        #-------------------
        if Actual_Time >= TempsdejeuxMarioBros:
        #if Actual_Time >= 20:
            continuer=0
            gagner=1
            '''print ("You win !")'''
            
##################################################################################################################################################################
#Invaders
    if gagner == 1:
        gagner=0
        
        pygame.init()       #initialisation de la bibliotèque pygame
        pygame.font.init()      #initialise les polices de caractère

        SCREEN_WIDTH, SCREEN_HEIGHT = 1024, 576  #on définit la taille de l'écran

        #SCREEN
        pygame.init()
        screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))  #taille de l'écran
        fond = pygame.image.load("images/fond.png").convert()   #mise en place de l'image du fond

        #LIMITE
        limite = pygame.image.load("images/limite.png").convert()
        limite_rect = limite.get_rect()
        limite_rect.midleft = ((0,476)) #positionner la limite invisible

        #VAISSEAU
        vaisseau = pygame.image.load("images/vaisseau.jpg").convert_alpha()   #insertion vaisseau
        vaisseau_rect = vaisseau.get_rect()      #on définit sa position qui va varier
        vaisseau_rect.midbottom = (SCREEN_WIDTH/2, SCREEN_HEIGHT)   #on le positionne à la moitié de l'écran

        #ALIEN
        alien = pygame.image.load("images/alien.jpg").convert_alpha() #insertion alien
        alien_rect = (pygame.Rect(randrange (0, 900), randrange (0, 200),79,54)) #on définit position aléatoire de l'alien

        #MISSILE
        missile = pygame.image.load("images/missile.jpg").convert_alpha()   #insertion missile
        missile_rect = missile.get_rect() #on définit sa position qui va varier
        missiles = []   #on ne met pas de limites de missile

        continuer = 1   #pour que l'écran reste

        #BOUCLE PRINCIPALE

        while continuer:
            alien_rect.bottom += 1  #donner vitesse initiale à l'alien
            #-------------------
            # Gestion des evenements
            #-------------------
            for event in pygame.event.get():
       
                if event.type == QUIT:    #si on appuie sur la croix l'écran se ferme
                    continuer = 0
         
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:   #si on appuie sur la touche echap l'écran se ferme
                        continuer = 0

                    elif event.key == K_SPACE:    #si on appuie sur espace...
                        missile_rect = missile_rect.copy()              #un missile s'ajoute 
                        missile_rect.midbottom = vaisseau_rect.midtop   #lance un missile
                        missiles.append( missile_rect )  #le missile ajouté prend la place de l'autre

            keep = pygame.key.get_pressed()  #on définit keep comme étant la fonction 'reste appuyé sur une touche'
            if keep[K_RIGHT]:                #si flèche droite
                vaisseau_rect.left += 5      #le vaisseau va se déplacer vers la droite
            if keep[K_LEFT]:                 #si flèche gauche
                vaisseau_rect.left -= 5      #le vaisseau va se déplacer vers la gauche

            if missile_rect.colliderect(alien_rect):   #s'il y a collision entre un missile et un alien
               alien_rect.midbottom = (2000,2000)      #alors l'alien change de position de telle sorte à ne plus être sur la surface et donc disparaitre
               alien_rect =(pygame.Rect(randrange (0, 900), randrange (0, 200),79,54)) #et un autre alien apparait à un autre endroit

            #Céation d'une fenètre lors de la défaite       
            if alien_rect.colliderect(limite_rect):         #si l'alien touche la limite
               '''print ("game over")'''          #game over s'écrit
               gagner=0
               continuer = 0

            #suite Invaders

            #-------------------
            # Gestion temps d execution
            #-------------------
            if Actual_Time >= TempsdejeuxInvaders:
                '''print("gagner!")'''
                gagner=1
                continuer = 0
            #-------------------

            #Suite Invaders
            vaisseau_rect.left = max(0, vaisseau_rect.left)                #limites pour que le vaisseau ne sorte pas de l'écran à gauche
            vaisseau_rect.right = min(SCREEN_WIDTH, vaisseau_rect.right)   #...à droite

            for missile_rect in missiles:  #lorsque un missile s'ajoute
                missile_rect.top -= 5      #on donne de la vitesse au premier missile

            screen.blit(fond, (0,0))  #on place le fond

            for missile_rect in missiles:
                screen.blit(missile, missile_rect)  #on place le missile

            screen.blit(vaisseau, vaisseau_rect)        #on place le vaisseau
            screen.blit(limite, limite_rect)
            screen.blit(alien, alien_rect)
            Actual_Time=chronometre(Start_Time) 
            aff_chrono(Actual_Time) # On affiche le temps
            pygame.display.flip()  #on rafraichit

##################################################################################################################################################################
#Message de FIN
                
    if gagner==1:
        #-------------------
        #Fenetre GAGNANT
        #-------------------
        pygame.init()       #initialisation de la bibliotèque pygame
        pygame.font.init()      #initialise les polices de caractère
        fenetre=pygame.display.set_mode((500,273),RESIZABLE)      #création d'une fenêtre avec possibilité de redimensionner

        pygame.display.set_caption("Tauryam")         #titre de la fenet

        # Remplissage de l'arrière-plan
        fond = pygame.image.load("images/fete-mario.jpg").convert()

        # Affichage d'un texte
        font = pygame.font.SysFont("Tahoma",20)
        text = font.render("You Win ! Bravo tu as délivré la maman de Tauryam !", 1, (253,241,184))
        textpos = text.get_rect()
        textpos.centerx = fond.get_rect().centerx
        textpos.centery = fond.get_rect().centery
        fond.blit(text, textpos)

        # Blitter le tout dans la fenêtre
        fenetre.blit(fond,(0,0))
        pygame.display.flip()       #rafraichir l'écran
        
        continuer=1
        while continuer:
            for event in pygame.event.get():
                if event.type == QUIT:    #si on appuie sur la croix l'écran se ferme
                    continuer = 0
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:   #si on appuie sur la touche echap l'écran se ferme
                        continuer = 0
        
    else:
        #-------------------
        #Fenetre PERDANT
        #-------------------
        #alien_rect.midbottom = (2000,2000)        #l'alien disparaît

        pygame.init()       #initialisation de la bibliotèque pygame
        pygame.font.init()      #initialise les polices de caractère
        fenetre=pygame.display.set_mode((500,309),RESIZABLE)      #création d'une fenêtre avec possibilité de redimensionner

        pygame.display.set_caption("Tauryam")         #titre de la fenet

        # Remplissage de l'arrière-plan
        fond = pygame.image.load("images/mort-inva.jpg").convert()

        # Affichage d'un texte
        font = pygame.font.SysFont("Tahoma",20)
        text = font.render("Tu es mort...",1, (253,241,184))
        textpos = text.get_rect()
        textpos.centerx = fond.get_rect().centerx
        textpos.centery = fond.get_rect().centery
        fond.blit(text, textpos)
      
        fenetre.blit(fond,(0,0))
        pygame.display.flip()       #rafraichir l'écran
        
        continuer=1
        while continuer:
            for event in pygame.event.get():
                if event.type == QUIT:    #si on appuie sur la croix l'écran se ferme
                    continuer = 0
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:   #si on appuie sur la touche echap l'écran se ferme
                        continuer = 0
        
        
 
finally:
    pygame.quit()       #fin du programme

#os.sytem("pause")
